FROM alpine
Maintainer Paul Weaver <paul.weaver@wipro.com>

RUN apk add --update python py-pip && rm -fr /var/cache/apk/*

WORKDIR app/
COPY app .

RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["python", "app.py"]
